import sublime_plugin, os

class JumpHeadImpl(sublime_plugin.TextCommand):
    def run(self, _):
        orig_file = self.view.file_name();
        if orig_file == None: return

        targ_file = None
        parts = orig_file.rsplit('.', 2)
        if parts[-1] == 'cpp' or parts[-1] == 'c':
            targ_file = parts[0] + ".hpp"
            if not os.path.exists(targ_file):
                targ_file = parts[0] + ".h"
        elif parts[-1] == 'hpp' or parts[-1] == 'h':
            targ_file = parts[0] + ".c"
            if not os.path.exists(targ_file):
                targ_file = parts[0] + ".cpp"
        else: return

        window = self.view.window()
        targ_view = window.open_file(targ_file)
        window.focus_view(self.view)
        orig_group = window.get_view_index(self.view)[0]
        if window.num_groups() > 1:
            if orig_group >= window.num_groups() - 1:
                target_group = orig_group - 1
            else:
                target_group = orig_group + 1
            window.set_view_index(targ_view, target_group, 0)
        window.focus_view(targ_view)
