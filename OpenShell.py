import sublime
import sublime_plugin
import os

class OpenShell(sublime_plugin.WindowCommand):
    def run(self):
        vars = self.window.extract_variables()
        path = vars.get("project_path")
        if path == None:
            path = vars.get("file_path")
            if path == None:
                path = "/"
        os.system("wt nt -d \"{}\"".format(path))
