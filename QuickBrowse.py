import sublime, sublime_plugin
from os import listdir
from os.path import isfile, isdir, join, expanduser, normpath, splitdrive

my_folder = "C:/users/haakon/documents/_sync/notes"
include_directories = True
include_parent_dir_entry = True
sort_directories_to_bottom = True

class QuickBrowse(sublime_plugin.WindowCommand):
    def open_path(self, path):
        file_names = listdir(path)

        options = []

        if include_directories:
            pred = (lambda entry: isdir(entry["path"])) if sort_directories_to_bottom else (lambda entry: not isdir(entry["path"]))
            self.table = sorted(
                map(lambda name: {
                        "name": name,
                        "path": normpath(join(path, name)),
                    },
                    file_names
                ),
                key=pred
            )

            if include_parent_dir_entry and len(splitdrive(path)[1]) > 1:
                parent_entry = {
                    "name": "..",
                    "path": normpath(join(path, ".."))
                }
                if sort_directories_to_bottom:
                    self.table.append(parent_entry)
                else:
                    self.table.insert(0, parent_entry)

            options = list(map(lambda entry: entry["name"] + ("/" if isdir(entry["path"]) else ""), self.table))
        else:
            options = list(filter(lambda name: isfile(join(path, name)), file_names))
            self.table = list(map(lambda name: {"path": join(path, name),}, options))

        self.window.show_quick_panel(options, self.on_done)

    def run(self):
        self.open_path(expanduser(my_folder))

    def on_done(self, index):
        if index == -1: return

        path = self.table[index]["path"]

        if not isdir(path):
            self.window.open_file(path)
        else:
            self.open_path(path)
